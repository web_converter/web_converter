var Lesson = sequelize.define(
    'lesson',
    {
        type:           { type: Sequelize.ENUM('Sem','Lect','Lab'), allowNull: false },
        start:          { type: Sequelize.DATE,                     allowNull: false },
        end:            { type: Sequelize.DATE,                     allowNull: false },
        description:    { type: Sequelize.TEXT,                     allowNull: true },
    },
    {
        tableName: 'lessons',
        timestamps: true,
        createdAt: 'created',
        updatedAt: 'updated',
        deletedAt: 'deleted',
    }
);

module.exports = Lesson;
