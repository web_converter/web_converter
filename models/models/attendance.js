var Attendance = sequelize.define(
    'attendance',
    {
        visited: {type: Sequelize.BOOLEAN, allowNull: false, defaultValue: false}
    },
    {
        tableName: 'attendance',
        timestamps: true,
        createdAt: 'created',
        updatedAt: 'updated',
        deletedAt: 'deleted',
    }
);

module.exports = Attendance;
