var LessonGroup = sequelize.define(
    'lesson_group',
    {},
    {
        tableName: 'lessons_groups',
        timestamps: true,
        createdAt: 'created',
        updatedAt: 'updated',
        deletedAt: 'deleted',
    }
);

module.exports = LessonGroup;
