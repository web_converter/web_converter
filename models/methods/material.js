'use strict'
const child_process = require('child-process-promise');
const pg = require('pg');
const username = app.config.postgres.user;
const password = app.config.postgres.password;
const database = app.config.postgres.database;
const host = app.config.postgres.host;

module.exports = function (models) {
    var Material = models.Material;

    Material.make = function (material_data, filename) {
        var ctx = {};
        material_data.material_name = filename;
        return sequelize.transaction(function (t) {
            return Material.unscoped().findOne({
                where: {title: material_data.title},
                transaction: t
            }).then(function (material) {
                if (material)
                    throw {message: 'MaterialExists'};
                return Material.create(material_data, {transaction: t});
            }).then(function(material){
                return material.dataValues;
            }).catch(function(err) {
                console.log('make material method err', err);
                throw err;
            });
        });
    }

    Material.download = function (material_data) {
        var ctx = {};
        return sequelize.transaction(function (t) {
            return Material.unscoped().findOne({
                where: {title: material_data.material_name},
                transaction: t
            }).then(function (material) {
                if (material)
                    throw {message: 'MaterialExists'};
                return Material.create(material_data, {transaction: t});
            }).then(function(material){
                return material.dataValues;
            }).catch(function(err) {
                console.log('make material method err', err);
                throw err;
            });
        });
    }
};
