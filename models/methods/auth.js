const uuid = require('node-uuid');


module.exports = function (models) {
    var Token = models.Token;
    var User = models.User;
    var Student = models.Student;
    var Group = models.Group;
    var Role = models.Role;


    Token.find_token = function (token, callback) {
        var result = {};

        return sequelize.transaction(function (t) {
            return Token.findOne({
                where: {
                    token: token
                },
                include: [
                    {
                        model: User,
                        required: true,
                        include: [{
                            model: Student,
                            include: [Group]
                        }]
                    },
                    {model: Role, required: true}
                ],
                transaction: t
            }).then(function (token) {
                if (token) {
                    token = token.dataValues;
                    result = token.user.dataValues;
                    result.permissions = token.role.dataValues.permissions;
                    result.token = token.token;
                    result.role = token.role.dataValues;
                    delete result.role.permissions;
                } else {
                    callback(null, null);
                    return;
                }
            }).then(function () {
                callback(null, result);
            }).catch(function (err) {
                callback(err, null);
            });
        });
    };


    Token.Instance.prototype.create_token = function () {
        this.token = uuid.v4();
    };
};
