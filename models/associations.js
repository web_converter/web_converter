module.exports = function (models) {
    const Token = models.Token;
    const Role = models.Role;
    const User = models.User;
    const CheckPoint = models.CheckPoint;
    const CheckPointGroup = models.CheckPointGroup;
    const TestAnswer = models.TestAnswer;
    const TestCase = models.TestCase;
    const TestCaseQuestion = models.TestCaseQuestion;
    const DataBase = models.DataBase;
    const Question = models.Question;
    const QuestionAnswer = models.QuestionAnswer;
    const Group = models.Group;
    const Student = models.Student;
    const Table = models.Table;
    const Lesson = models.Lesson;
    const LessonGroup = models.LessonGroup;
    const Attendance = models.Attendance;

    CheckPoint.belongsTo(User,              { foreignKey: { name: 'owner_id',       allowNull: false }});
    CheckPoint.hasMany(QuestionAnswer,      { foreignKey: { name: 'check_point_id', allowNull: true }, as: 'answers', onDelete: 'CASCADE' });
    CheckPoint.hasMany(TestAnswer,          { foreignKey: { name: 'check_point_id', allowNull: true }, as: 'tests_answers', onDelete: 'CASCADE' });
    CheckPoint.hasMany(TestCase,            { foreignKey: { name: 'check_point_id', allowNull: true }, as: 'test_cases', onDelete: 'CASCADE' });
    CheckPoint.hasMany(CheckPointGroup,     { foreignKey: { name: 'check_point_id', allowNull: true }, as: 'groups', onDelete: 'CASCADE' });

    CheckPointGroup.belongsTo(CheckPoint,   { foreignKey: { name: 'check_point_id', allowNull: false }});
    CheckPointGroup.belongsTo(Group,        { foreignKey: { name: 'group_id',       allowNull: false }});

    DataBase.belongsTo(User,                { foreignKey: { name: 'owner_id',       allowNull: false }});
    DataBase.hasMany(Table,                 { foreignKey: { name: 'db_id',          allowNull: false }, as: 'tables', onDelete: 'CASCADE' });
    DataBase.hasMany(Question,              { foreignKey: { name: 'db_id',          allowNull: false }, as: 'questions', onDelete: 'CASCADE' });

    Group.hasMany(Student,                  { foreignKey: { name: 'group_id',       allowNull: false }, as: 'students', onDelete: 'CASCADE' });
    Group.hasMany(CheckPointGroup,          { foreignKey: { name: 'group_id',       allowNull: false }, as: 'check_points', onDelete: 'CASCADE' });
    Group.hasMany(LessonGroup,              { foreignKey: { name: 'group_id',       allowNull: false }, as: 'lessons' });

    Role.hasMany(Token,                     { foreignKey: { name: 'role_id',        allowNull: false }});

    Student.belongsTo(Group,                { foreignKey: { name: 'group_id',       allowNull: false }});
    Student.hasOne(User,                    { foreignKey: { name: 'student_id',     allowNull: true }, as: 'user', onDelete: 'CASCADE' });

    Table.belongsTo(DataBase,               { foreignKey: { name: 'db_id',          allowNull: false }});

    Token.belongsTo(User,                   { foreignKey: { name: 'user_id',        allowNull: false }});
    Token.belongsTo(Role,                   { foreignKey: { name: 'role_id',        allowNull: false }});

    TestAnswer.belongsTo(User,              { foreignKey: { name: 'user_id',        allowNull: false }});
    TestAnswer.belongsTo(CheckPoint,        { foreignKey: { name: 'check_point_id', allowNull: false }});
    TestAnswer.belongsTo(TestCase,          { foreignKey: { name: 'test_case_id',   allowNull: false }});

    TestCase.belongsTo(CheckPoint,          { foreignKey: { name: 'check_point_id', allowNull: false }});
    TestCase.hasMany(TestCaseQuestion,      { foreignKey: { name: 'test_case_id',   allowNull: true }, as: 'questions', onDelete: 'CASCADE' });
    TestCase.hasMany(TestAnswer,            { foreignKey: { name: 'test_case_id',   allowNull: true }, as: 'tests_answers', onDelete: 'CASCADE' });

    TestCaseQuestion.belongsTo(TestCase,    { foreignKey: { name: 'test_case_id',   allowNull: false }});
    TestCaseQuestion.belongsTo(Question,    { foreignKey: { name: 'question_id',    allowNull: false }});

    Question.belongsTo(DataBase,            { foreignKey: { name: 'db_id',          allowNull: true }, onDelete: 'CASCADE' });
    Question.belongsTo(User,                { foreignKey: { name: 'owner_id',       allowNull: false }});
    Question.hasMany(QuestionAnswer,        { foreignKey: { name: 'question_id',    allowNull: false }, as: 'answers', onDelete: 'CASCADE' });
    Question.hasMany(TestCaseQuestion,      { foreignKey: { name: 'question_id',    allowNull: false }, as: 'test_cases', onDelete: 'CASCADE' });

    QuestionAnswer.belongsTo(User,          { foreignKey: { name: 'user_id',        allowNull: false }});
    QuestionAnswer.belongsTo(Question,      { foreignKey: { name: 'question_id',    allowNull: false }});
    QuestionAnswer.belongsTo(CheckPoint,    { foreignKey: { name: 'check_point_id', allowNull: true }});

    User.hasMany(DataBase,                  { foreignKey: { name: 'owner_id',       allowNull: false }});
    User.hasMany(Question,                  { foreignKey: { name: 'owner_id',       allowNull: false }});
    User.belongsTo(Role,                    { foreignKey: { name: 'role_id',        allowNull: false }});
    User.hasMany(Token,                     { foreignKey: { name: 'user_id',        allowNull: false }});
    User.hasMany(QuestionAnswer,            { foreignKey: { name: 'user_id',        allowNull: false }, as: 'answers', onDelete: 'CASCADE' });
    User.hasMany(TestAnswer,                { foreignKey: { name: 'user_id',        allowNull: false }, as: 'tests_answers', onDelete: 'CASCADE' });
    User.belongsTo(Student,                 { foreignKey: { name: 'student_id',     allowNull: true }});
    User.hasMany(Attendance,                { foreignKey: { name: 'creator_id',     allowNull: true }, as: 'creator' });
    User.hasMany(Attendance,                { foreignKey: { name: 'visitor_id',     allowNull: false }, as: 'visitor' });

    Lesson.hasMany(LessonGroup,             { foreignKey: { name: 'lesson_id',      allowNull: false }});
    Lesson.hasMany(Attendance,              { foreignKey: { name: 'lesson_id',      allowNull: false }});

    LessonGroup.belongsTo(Lesson,           { foreignKey: { name: 'lesson_id',      allowNull: false }});
    LessonGroup.belongsTo(Group,            { foreignKey: { name: 'group_id',       allowNull: false }});
    LessonGroup.hasMany(Attendance,         { foreignKey: { name: 'lesson_group_id',allowNull: false }});

    Attendance.belongsTo(Lesson,            { foreignKey: { name: 'lesson_id',      allowNull: false }});
    Attendance.belongsTo(LessonGroup,       { foreignKey: { name: 'lesson_group_id',allowNull: false }});
    Attendance.belongsTo(User,              { foreignKey: { name: 'creator_id',     allowNull: true }, as: 'creator'});
    Attendance.belongsTo(User,              { foreignKey: { name: 'visitor_id',     allowNull: false }, as: 'visitor'});
};
