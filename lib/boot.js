'use strict'

const express = require('express');
const path = require('path');
const sgMail = require('@sendgrid/mail');

const http = express();
/*подключение контроллеров*/

module.exports = function (options) {

    const EventEmitter = require('events').EventEmitter;
    var app = new EventEmitter();
    sgMail.setApiKey(options.config.mail.api_key);

    app.sgMail = sgMail;
    app.root_dir = options.root_dir;
    app.config = {};
    app.config.express = options.config.express;
    app.config.postgres = options.config.postgres;


    require('./express.js')(app);

    return app;
}
