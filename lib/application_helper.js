global.ApplicationHelper = {
    count_pages: function(docs, limit) {
        if (!docs || docs < limit) {
            return 1;
        } else if (docs % limit === 0) {
            return docs / limit;
        } else {
            return Math.floor(docs / limit) + 1;
        }
    },

    map_title: function (title, max_len) {
        if (title.length > max_len) {
            return title.slice(0, max_len - 2) + '...' + title.substring(title.length - max_len + 1);
        } else {
            return title;
        }
    },

    get_max_mark: function (checkpoint) {
        if (checkpoint.test_config.test_cases_amount !== '') {
            return Number(checkpoint.test_config.questions_amount) * 5; //5 потому что оценивается всегда либо в 0, либо в 5
            //TODO переделать проверку ответов, чтобы возвращала бал в соответствии со сложностью вопроса
        } else {
            const a = Number(checkpoint.test_config.start_complexity),
                c = Number(checkpoint.test_config.great_complexity),
                n = Number(checkpoint.test_config.questions_amount);
            return (c - a + 1) * ((a + c) / 2) + c * (n - (c - a + 1));
        }
    },
};
