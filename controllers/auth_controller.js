const moment = require('moment');

var getMessage = function(msg) {
    switch (msg) {
        case 'InvalidIp':
            return 'Неверный IP';
        default:
            return 'Неверный логин или пароль';
    }
};

var get = {
    '/login': function(req, res, next) {
        res.render('login');
    },

    '/recovery': function(req, res, next) {
        res.render('main/recover');
    },

    '/registry': function (req, res, next) {
        return app.Group.findAll({
            where: {
                created: {  $gt: moment().startOf('year').toDate().getTime() }
            },
            attributes: ['id', 'title']
        }).then((groups) => {
            res.render('main/registry', {
                groups: groups
            });
        }).catch((error) => {
            console.log(error);
            next(error);
        })

    },

    '/logout': function(req, res, next) {
        req.session.destroy();
        res.clearCookie('session');
        res.redirect('/login');
    }
};

var post = {
    '/login': function(req, res, next) {
        var username = req.body.username,
            password = req.body.password,
            ctx = {};

        return app.User.findOne({
            where: {login: username},
            include: [{
                model: app.Role
            }]
        }).then(function(user) {
            if (!user) {
                throw {message: 'AuthError', desc: 'InvalidLogin'};
            } else {
                ctx.user = user.dataValues;
                ctx.role = user.dataValues.role;

                return user.validate_password(password);
            }
        }).then(function(valid) {
            if (!valid) {
                throw {message: 'AuthError', desc: 'InvalidPassword'};
            } else {
                var token = app.Token.build({
                    role_id: ctx.user.role_id,
                    user_id: ctx.user.id
                });

                token.create_token();
                return token.save();
            }
        }).then(function(token) {
            req.session.token = token.token;
            res.success({});
        }).catch(function(err) {
            console.log('err', err);
            req.flash('error', "Неверный логин или пароль");
            res.error('AuthError');
        });

    },

    '/registry': async function (req, res) {
        let options = {
                user_data: {},
                student_data: {}
            },
            is_valid = true;

        if (req.body.login) {
            let user = await app.User.findOne({
                where: { login: req.body.login }
            });
            if (user) {
                is_valid = false;
            } else {
                options.user_data = {
                    login: req.body.login,
                    role: 'unconfirmed',
                    code: Math.floor(Math.random() * 900000 + 100000)
                }

            }
        }

        if (req.body.name) {
            options.user_data.name = req.body.name;
        }

        if (req.body.email) {
            options.user_data.email = req.body.email;
        }

        if (req.body.password) {
            options.user_data.password = req.body.password;
        }

        if (req.body.group) {
            options.student_data = {
                group_id: req.body.group
            };
        }

        if (is_valid) {
            if (req.body.code) {
                return app.User.findOne({
                    where: { code: Number(req.body.code) }
                }).then((user) => {
                    if (!user) {
                        throw { message: "Неверный код" };
                    }
                    user.code = null;
                    user.role_id = 3;
                    return user.save();
                }).then((user) => {
                    res.success({})
                }).catch((error) => {
                    res.error({ message: error });
                });
            } else {
                return app.Student.make(options).then((student) => {
                    let msg = {
                        to: req.body.email,
                        from: 'kir.feo97@yandex.ru',
                        subject: 'no-reply',
                        html: `
                    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
<!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width" name="viewport"/>
<!--[if !mso]><!-->
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<!--<![endif]-->
<title></title>
<!--[if !mso]><!-->
<!--<![endif]-->
<style type="text/css">
body {
margin: 0;
padding: 0;
}

table,
td,
tr {
vertical-align: top;
border-collapse: collapse;
}

* {
line-height: inherit;
}

a[x-apple-data-detectors=true] {
color: inherit !important;
text-decoration: none !important;
}
</style>
<style id="media-query" type="text/css">
@media (max-width: 520px) {

.block-grid,
.col {
min-width: 320px !important;
max-width: 100% !important;
display: block !important;
}

.block-grid {
width: 100% !important;
}

.col {
width: 100% !important;
}

.col>div {
margin: 0 auto;
}

img.fullwidth,
img.fullwidthOnMobile {
max-width: 100% !important;
}

.no-stack .col {
min-width: 0 !important;
display: table-cell !important;
}

.no-stack.two-up .col {
width: 50% !important;
}

.no-stack .col.num4 {
width: 33% !important;
}

.no-stack .col.num8 {
width: 66% !important;
}

.no-stack .col.num4 {
width: 33% !important;
}

.no-stack .col.num3 {
width: 25% !important;
}

.no-stack .col.num6 {
width: 50% !important;
}

.no-stack .col.num9 {
width: 75% !important;
}

.video-block {
tmax-width: none !important;
}

.mobile_hide {
min-height: 0px;
max-height: 0px;
max-width: 0px;
display: none;
overflow: hidden;
font-size: 0px;
}

.desktop_hide {
display: block !important;
max-height: none !important;
}
}
</style>
</head>
<body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #FFFFFF;">
<!--[if IE]><div class="ie-browser"><![endif]-->
<table bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="table-layout: fixed; vertical-align: top; min-width: 320px; Margin: 0 auto; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #FFFFFF; width: 100%;" valign="top" width="100%">
<tbody>
<tr style="vertical-align: top;" valign="top">
<td style="word-break: break-word; vertical-align: top;" valign="top">
<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" style="background-color:#FFFFFF"><![endif]-->
<div style="background-color:transparent;">
<div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 500px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
<div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:500px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
<!--[if (mso)|(IE)]><td align="center" width="500" style="background-color:transparent;width:500px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
<div class="col num12" style="min-width: 320px; max-width: 500px; display: table-cell; vertical-align: top; width: 500px;">
<div style="width:100% !important;">
<!--[if (!mso)&(!IE)]><!-->
<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
<!--<![endif]-->
<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
<div style="color:#555555;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
<div style="line-height: 1.2; font-size: 12px; color: #555555; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14px;">
<p style="font-size: 17px; line-height: 1.2; word-break: break-word; mso-line-height-alt: 20px; mso-ansi-font-size: 18px; margin: 0;"><span style="font-size: 17px; mso-ansi-font-size: 18px;"><strong>Вы были зарегистрированы в системе Web - Converter</strong></span></p>
<p style="font-size: 14px; line-height: 1.2; word-break: break-word; mso-line-height-alt: 17px; margin: 0;"> </p>
<p style="font-size: 17px; line-height: 1.2; word-break: break-word; mso-line-height-alt: 20px; mso-ansi-font-size: 18px; margin: 0;"><span style="font-size: 17px; mso-ansi-font-size: 18px;">Учетные данные:</span></p>
<p style="font-size: 17px; line-height: 1.2; word-break: break-word; mso-line-height-alt: 20px; mso-ansi-font-size: 18px; margin: 0;"><span style="font-size: 17px; mso-ansi-font-size: 18px;"><strong>Login: ${req.body.login}</strong></span></p>
<p style="font-size: 17px; line-height: 1.2; word-break: break-word; mso-line-height-alt: 20px; mso-ansi-font-size: 18px; margin: 0;"><span style="font-size: 17px; mso-ansi-font-size: 18px;"><strong>Password: ${req.body.password}</strong></span></p>
<p style="font-size: 14px; line-height: 1.2; word-break: break-word; mso-line-height-alt: 17px; margin: 0;"> </p>
<p style="font-size: 17px; line-height: 1.2; word-break: break-word; mso-line-height-alt: 20px; mso-ansi-font-size: 18px; margin: 0;"><span style="font-size: 17px; mso-ansi-font-size: 18px;">Для подтверждения аккаунта введите код:</span></p>
<p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 17px; margin: 0;"><strong><span style="font-size: 18px;">${options.user_data.code}</span></strong></p>
</div>
</div>
<!--[if mso]></td></tr></table><![endif]-->
<!--[if (!mso)&(!IE)]><!-->
</div>
<!--<![endif]-->
</div>
</div>
<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
</div>
</div>
</div>
<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
</td>
</tr>
</tbody>
</table>
<!--[if (IE)]></div><![endif]-->
</body>
</html>
                    `
                    }
                    return app.sgMail.send(msg).then(() => {}, error => {
                        console.error(error);

                        if (error.response) {
                            console.error(error.response.body)
                        }
                    });
                }).then((mail) => {
                    res.success({mail: true});
                }).catch((error) => {
                    res.error({ message: error });
                });
            }
        } else {
            res.error({ message: "Пользователь с таким логином уже существует" });
        }
    },

    '/recovery': function (req, res) {
        let ctx = {};

        console.log(req.body);

        return app.User.findOne({
            where: { email: req.body.email }
        }).then((user) => {
            if (!user) {
                throw "Пользователь с таким email не зарегистрирован";
            }
            ctx.user = user;
            return ctx.user.generate_password();
        }).then(() => {
            ctx.password = ctx.user.password;
            return ctx.user.generate_salt();;
        }).then((user) => {
            return ctx.user.hash_password();
        }).then((user) => {
            return ctx.user.save();
        }).then(() => {
            return app.sgMail.send({
                to: req.body.email,
                from: 'kir.feo97@yandex.ru',
                subject: 'no-reply',
                text: 'Новый пароль: ' + ctx.password
            }).then(() => {}, error => {
                console.error(error);

                if (error.response) {
                    console.error(error.response.body)
                }
            });
        }).then(() => {
            res.success({ mail: true });
        }).catch((error) => {
            console.log(error);
            res.error({ message: error });
        });
    }
};

module.exports = {
    resource: '',
    methods: {
        get: get,
        post: post
    }
};
