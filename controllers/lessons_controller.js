const count_pages = ApplicationHelper.count_pages;

var get = {
    '/': function (req, res) {
        res.render('lessons/index');
    },
    '/table': function (req, res) {
        var options = {},
            skip = 0,
            limit = 15,
            page = Number(req.query.page) || 1;

        if (page > 1)
            skip = limit * (page - 1);

        return app.Lesson.findAndCountAll({
            where: options,
            distinct: true,
            order: 'start DESC',
            include: [
                {
                    model: app.LessonGroup,
                    include: [
                        {
                            model: app.Group
                        }
                    ]
                },
            ],
            limit: limit,
            offset: skip,
        }).then(function (lessons) {
            var pages = count_pages(lessons.count, limit),
                pages_min = (page - 3 < 1) ? 1 : page - 3,
                pages_max = (pages_min + 6 > pages) ? pages : pages_min + 6;
            res.render('lessons/table',{
                lessons: lessons.rows,
                page: page,
                pages: pages,
                pages_min: pages_min,
                pages_max: pages_max
            });
        }).catch(function (error) {
            console.log(error);
            res.error(error);
        });


    },
    '/add': function (req, res) {
        return app.Group.findAll().then((groups) => {
            res.render('lessons/add',{
                groups: groups
            });
        });
    }
};

var post = {
    '/add': function (req, res) {
        var lesson_id = null;
        return app.Lesson.create({
            type: req.body.type,
            start: new Date(req.body.start),
            end: new Date(req.body.end),
            description: req.body.description
        }).then(function (lesson) {
            lesson_id = lesson.id;
            return Promise.all(req.body.groups.map(function (group) {
                return app.LessonGroup.create({
                    lesson_id: lesson.id,
                    group_id: Number(group)
                });
            }));
        }).then(function (lesson_groups) {
            return Promise.all(lesson_groups.map(function (lesson_group) {
                return app.User.findAll({
                    include: [
                        {
                            model: app.Student,
                            where: {
                                group_id: lesson_group.group_id
                            }
                        }
                    ]
                }).then(function (users) {
                    return Promise.all(users.map(function (user) {
                        return app.Attendance.create({
                            creator_id: null,
                            visitor_id: user.id,
                            lesson_id: lesson_id,
                            lesson_group_id: lesson_group.id
                        });
                    }));
                });
            }));
        }).then(function () {
            res.success({lesson_id: lesson_id});
        }).catch(function (error) {
            console.log(error);
            res.error(error);
        });
    }
};

var put = {

};

var _delete = {

};

module.exports = {
    resource: 'Lesson',
    methods: {
        get: get,
        post: post,
        put: put,
        delete: _delete
    }
};
