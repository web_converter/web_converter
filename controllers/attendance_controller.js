const moment = require('moment');

let get = {
    '/': function (req, res) {
        return app.Group.findAll().then(function (groups) {
            res.render('attendance/index', {
                groups: groups
            });
        }).catch(function (error) {
            res.error(error);
        });
    },

    '/table': function (req, res) {
        let options = {};

        if (req.query.group_id) {
            options.group_id = Number(req.query.group_id);
        } else {
            options.group_id = Number(req.user.student.group.id);
        }

        if (req.user.role.id !== 1) {
            options.visited = false;
        }

        return app.Attendance.findAll({
            /*where: {
                visited: options.visited
            },*/
            include: [
                {
                    model: app.LessonGroup,
                    where: {
                        group_id: options.group_id
                    },
                    include: [
                        {
                            model: app.Lesson,
                            where: {
                                start: {
                                    $lte: new Date().getTime()
                                },
                                end: {
                                    $gte: new Date().getTime()
                                }
                            }
                        }
                    ]
                },
                {
                    model: app.User,
                    as: 'visitor'
                }
            ]
        }).then(function (attendance) {
            res.render('attendance/table', {
                visitors: attendance
            });
        }).catch(function (error) {
            console.log(error);
            res.error({ error: error });
        });
    },

    '/students': function (req, res) {
        let ctx = {
            attendance_block: []
        };

        return app.Lesson.findAll({
            include: [
                {
                    model: app.LessonGroup,
                    include: [
                        {
                            model: app.Group,
                            include: [
                                {
                                    model: app.Student,
                                    as: 'students',
                                    where: { id: Number(req.query.student_id)}
                                }
                            ]
                        },
                        {
                            model: app.Attendance,
                            include: [{
                                model: app.User,
                                as: 'visitor',
                                where: { student_id: Number(req.query.student_id)}
                            }]
                        }
                    ]
                }
            ]
        }).then(function (lessons) {
            let data = lessons.map(lesson => ({
                date: moment(lesson.start).format('MM.DD'),
                raw_date: lesson.start,
                type: lesson.type,
                visited: lesson.lesson_groups[0].attendances[0].dataValues.visited
            })).reduce((acc, cur) => {
                let old = acc.find(elem => elem.date === cur.date);

                if (old) {
                    old.lessons.push({type: cur.type, visited: cur.visited});
                    return acc;
                } else {
                    return [...acc, {date: cur.date, raw_date: cur.raw_date, lessons: [{type: cur.type, visited: cur.visited}]}];
                }
            }, []).map(day => ({
                date: day.date,
                raw_date: day.raw_date,
                visited: day.lessons.filter(e => e.visited).length / day.lessons.length,
            })).map(day => ({
                date: day.date,
                visited: moment(day.raw_date) > moment() ? '#808080'
                    : day.visited > 0.8 ? '#69b861'
                        : day.visited > 0.3 ? '#e4d200' : '#db5053'
            }));

            for (let i = 0; i < data.length; i = i+6) {
                ctx.attendance_block.push([...data.slice(i, i + 6)]);
            }

            console.log(ctx.attendance_block);

            res.render('students/attendance', {
                attendance_block: ctx.attendance_block
            });
        }).catch(function (err) {
            console.log(err);
            res.error(err);
        });
    },
};

let post = {
    '/': function (req, res) {

    },
};

let put = {
    '/check_in': function (req, res) {
        let visitor_ids = req.body.visitor_ids;
        return app.Attendance.update({
            visited: true,
            creator_id: Number(req.user.id)
        },{
            where: { id: visitor_ids }
        }).then(function (result) {
            res.success({update: result});
        }).catch(function (error) {
            console.log(error);
            res.error({error: error});
        });
    }
};

module.exports = {
    resource: 'Attendance',
    methods: {
        get: get,
        post: post,
        put: put,
    }
};
