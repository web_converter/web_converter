

var get = {
    '/': function(req, res, next) {
        var options = {},
            groups_options = {},
            ctx = {};

        if (req.user.role.role === 'student') {
            groups_options = { group_id: req.user.student.group.id };
        }

        return app.CheckPoint.findAll({
            where: options,
            include: [{
                model: app.CheckPointGroup,
                as: 'groups',
                where: groups_options
            }]
        }).then(function(check_points) {
            ctx.check_points = check_points;

            return app.Lesson.findAll({
                include: [{
                    model: app.LessonGroup,
                    where: groups_options
                }]
            });
        }).then(function (lessons) {
            res.render('main/index', {
                check_points: ctx.check_points,
                lessons: lessons,
                current_page: 'main'
            });
        }).catch(function(err) {
            res.error(err);
            next();
        });
    },

    '/profile': function(req, res, next) {
        let id = Number(req.user.id);

        return app.User.findOne({
            where: {id: id},
            include: [{
                model: app.Student,
                include: [{
                    model: app.Group
                }]
            }],
        }).then(function (student) {

            if (!student) {
                throw {message: 'NotFound'};
            } else {
                res.render('students/show', { student: student.dataValues });
            }
        }).catch(function (err) {
            console.log('err', err);
            res.error(err);
        });
    },
};

module.exports = {
    resource: '',
    methods: {
        get: get,
    }
};
