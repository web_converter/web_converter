$(document).ready(function () {
    $('#submit_btn').click(function (e) {
        e.preventDefault();
        e.stopPropagation();

        if ($('#email').val()) {
            let data = {
                email: $('#email').val()
            }

            $.ajax({
                type: 'POST',
                url: '/recovery',
                data: data,
                dataType: 'json',
                async: false
            }).done((res) => {
                if (res.success) {
                    $('.card-block').html(`
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="text-xs-center font-small-3">Вам на почту было направлено письмо с кодом подтверждения.</p>
                        </div>
                    </div>
                `);

                    setTimeout(() => {
                        window.location = '/login'
                    }, 3000);
                } else {
                    $('#code_errors').text(res.error).removeClass('hidden');
                }
            }).fail((error) => {
                $('#code_errors').text(error).removeClass('hidden');
            });
        } else {
            $('#code_errors').text("Укажите почту").removeClass('hidden');
        }
    })
});
