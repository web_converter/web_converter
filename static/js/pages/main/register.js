$(document).ready(function () {
    $('.selectize-select').selectize({
        create: true,
        dropdownParent: 'body'
    });

    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

    $('#skip').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('.card-block').html(`
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="text-xs-center font-small-3">Вам на почту было направлено письмо с кодом подтверждения.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3">
                            <p class="float-xs-right">Код</p>
                        </div>
                        <div class="col-xs-9">
                            <input id="code" class="form-control" type="text" minlength="4">
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="col-xs-9 offset-xs-3">
                            <button id="code_btn" class="btn btn-block btn-success btn-darken-2"> Подтвердить
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-9 offset-xs-3">
                            <p id="code_errors" class="font-small-3 text-danger hidden"> 
                        </div>
                    </div>
                `);

        $('#code_btn').click(function () {
            let $self = this;
            let code_data = {
                code: $('#code').val()
            };

            $.ajax({
                type: 'POST',
                url: '/registry',
                data: code_data,
                dataType: 'json',
                async: false
            }).done((res) => {
                if (res.success) {
                    window.location = '/';
                } else {
                    $('#code_errors').text(res.error.message).removeClass('hidden');
                }
            }).fail(error => console.log);
        });
    })

    $('#submit_btn').click(function (e) {
        e.preventDefault();
        $('#errors').addClass('hidden');
        let $self = this;

        if ($('#password').val() === $('#repassword').val() && document.forms['form_registry'].checkValidity()) {
            $($self).addClass('disabled');
            let data = {
                name:       $('#name').val(),
                login:      $('#login').val(),
                password:   $('#password').val(),
                email:      $('#email').val(),
                group:      $('#group').val()
            };

            $.ajax({
                type: 'POST',
                url: '/registry',
                data: data,
                dataType: 'json',
                async: false
            }).done(function (res) {
                if (res.success) {
                    $($self).removeClass('disabled');
                    $('.card-block').html(`
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="text-xs-center font-small-3">Вам на почту было направлено письмо с кодом подтверждения.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3">
                            <p class="float-xs-right">Код</p>
                        </div>
                        <div class="col-xs-9">
                            <input id="code" class="form-control" type="text" minlength="4">
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="col-xs-9 offset-xs-3">
                            <button id="code_btn" class="btn btn-block btn-success btn-darken-2"> Подтвердить
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-9 offset-xs-3">
                            <p id="code_errors" class="font-small-3 text-danger hidden"> 
                        </div>
                    </div>
                `);
                } else {
                    $($self).removeClass('disabled');
                    $('#errors').text(res.error).removeClass('hidden');
                }
            }).fail((error) => {
                console.log(error);
            }).always(() => {
                $('#code_btn').click(function () {
                    let $self = this;
                    let code_data = {
                        code: $('#code').val()
                    };

                    $.ajax({
                        type: 'POST',
                        url: '/registry',
                        data: code_data,
                        dataType: 'json',
                        async: false
                    }).done((res) => {
                        if (res.success) {
                            window.location = '/';
                        } else {
                            $('#code_errors').text(res.error).removeClass('hidden');
                        }
                    }).fail(error => console.log);
                });
            });
        } else {
            $('#errors').text("Поля заполнены неверно").removeClass('hidden');
        }
    });
});

function formValidate(form, options) {
    $(form).find()
    form.valid = function () {

    }
}
