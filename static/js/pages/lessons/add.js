$(document).ready(function () {
    var start,
        end;
    $dates = $('#start, #end');
    $dates.datetimepicker({
        format: 'DD.MM.YYYY HH:mm',
        locale: 'ru',
        icons: {
            time: 'icon-alarm',
            date: 'icon-calendar3',
            up: 'icon-chevron-up',
            down: 'icon-chevron-down',
            previous: 'icon-chevron-left',
            next: 'icon-chevron-right',
            today: 'icon-crosshairs',
            clear: 'icon-bin',
            close: 'icon-close'
        }
    });

    $('.select2').select2({
        containerCssClass: 'select',
        language: {
            maximumSelected: function() {
                return "Вы не можете выбрать более одного элемента";
            },
            noResult: function() {
                return "Нет данных для выбранных фильтров";
            }
        },
    });

    $('#start').data('DateTimePicker').date(moment().startOf('day').add(24 * 7, 'h'));
    $('#end').data('DateTimePicker').date(moment().startOf('day').add(25 * 7, 'h'));

    start = moment($('#start').data('DateTimePicker').date()).format("YYYY-MM-DD HH:mm:ss");
    end = moment($('#end').data('DateTimePicker').date()).format("YYYY-MM-DD HH:mm:ss");

    $('#start').on('dp.change', function(e) {
        start = moment(new Date($('#start').data('DateTimePicker').date())).format("YYYY-MM-DD HH:mm:ss");
    });

    $('#end').on('dp.change', function(e) {
        end = moment(new Date ($('#end').data('DateTimePicker').date())).format("YYYY-MM-DD HH:mm:ss");
    });

    $('#new_lesson').submit(function (event) {
        event.preventDefault();
        var data = {
            start: start,
            end: end,
            type: $('#type').val(),
            groups: $('.select2').val(),
            description: $('#description').val(),
        };
        $('#submit').prop('disabled', true);
        $('#submit').html('<i class="icon-spinner12"></i>');
        console.log(data);
        $.ajax({
            type: 'POST',
            url: '/lessons/add',
            data: data,
            dataType: 'json',
            async: true
        }).done(function (res) {
            console.log(res);
            if (res.success) {
                bootbox.dialog({
                    className: 'slideInDown',
                    message: `Занятие <a class="alert-link" href=/lessons/${res.lesson_id}>${res.lesson_id}</a> создано`,
                    buttons: {
                        'back_to_list': {
                            label: 'Вернутся к списку занятий',
                            className: 'btn-default mr-1',
                            callback: function() { window.location.assign('/lessons'); }
                        },
                        'create_new_one': {
                            label: 'Создать еще одно',
                            className: 'btn-success',
                            callback: function() {
                                bootbox.hideAll();
                                return false;
                            }
                        }
                    }
                });
            } else {
                bootboxError(res.error);
                return false;
            }
        }).fail(function () {
            bootboxError('Запрос не был обработан сервером');
            return false;
        });
    });
});