$(document).ready(function () {
    let options = {};
    $(document).on('click', '#lessons_table ul.pagination a.page-link', function(e) {
        options.page = e.target.innerHTML;
        getTable('/lessons/table', options, '#lessons_table');
    });
});
