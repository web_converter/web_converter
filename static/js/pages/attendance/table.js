$(document).ready(function () {
    var data = {
        visitor_ids: []
    };
    $('.check-in').map(function () {
        this.addEventListener('click', function () {
            data.visitor_ids.push(Number(this.value));
        });
    });

    $('#check').on('click', function () {
        bootbox.dialog({
            className: 'slideInDown',
            message: 'Подтверждая действие Вы становитесь ответственным за внесенные изменения',
            buttons: {
                'create_new_one': {
                    label: 'Подтвердить',
                    className: 'btn-success mr-1',
                    callback: function() {
                        $.ajax({
                            type: 'PUT',
                            url: '/attendance/check_in',
                            data: data,
                            dataType: 'json',
                            async: true
                        }).done(function (res) {
                            if (res.success) {
                                bootbox.dialog({
                                    className: 'slideInDown',
                                    message: 'Изменения успешно сохранены',
                                    buttons: {
                                        'confirm': {
                                            label: 'Закрыть',
                                            className: 'btn-default',
                                            callback: function () {
                                                return true;
                                            }
                                        }
                                    }
                                });
                            } else {
                                bootboxError(res.error);
                                return false;
                            }
                        }).fail(function () {
                            bootboxError('Запрос не был обработан сервером');
                            return false;
                        });
                    }
                },
                'back_to_list': {
                    label: 'Отмена',
                    className: 'btn-default',
                    callback: function() {
                        bootbox.hideAll();
                        return false;
                    }
                }
            }
        });
    });
});
