$(document).ready(function () {
    getTable('attendance/table', {}, '#attendance_table', function () {});

    $('#group_select').on('change', function () {
        getTable('attendance/table', { group_id: this.value }, '#attendance_table', function () {});
    });
});
