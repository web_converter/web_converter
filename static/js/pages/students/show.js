$(document).ready(function() {
    // TODO partner_id объявлен в шаблоне!
    var options = {
        user_id: window.user_id || null,
        student_id: window.student_id || null,
        group_id: window.group_id || null,
    };
    getTable('/attendance/students', options, '#attendance_table', function () {});
    getTable('/questions_answers/preparing', options, '#preparing_table', function () {});
    getTable('/check_points/grid', options, '#check_point_grid', () => {});
    $.get('/questions_answers/stats?student_id=' + options.student_id + '&user_id=' + options.user_id, function (res) {
        if (res.success) {
            setTimeout(() => {window.drawPie(res.stats.ra, res.stats.tp, res.stats.sql);}, 1000);
        } else {
            console.log(res.error);
        }
    });

    $('#preparing_table').on('click', 'ul.pagination a.page-link', function(e) {
        options.page = e.target.innerHTML;
        getTable('/questions_answers/preparing', options, '#preparing_table');
    });

    var $editable = $('.editable');

    $editable.editable({
        ajaxOptions: { type:'PUT' },
        type: 'text',
        mode: 'inline',
        url: '/users/' + user_id,
        pk: 1, //иначе не уходит ajax
        emptytext: 'Не задано',
        params: function(params) {
            var obj = {};

            if (params.value === '') {
                obj[params.name] = '';
            } else {
                obj[params.name] = params.value;
            }

            return obj;
        },
        success: function(res) {
            //window.location.reload();
        },
        error: function(res) { console.log(res); },
        validate: function(value) {
            if (!($(this).data('name') == 'specialty') && $.trim(value) == '') {
                return 'Необходимо заполнить данное поле';
            }
        }
    });

    $editable.editable('toggleDisabled');

    $('#edit').click(function() {
        var $this = $(this);

        if ($this.attr('data-edit') === 'enable') {
            //off
            $this.removeClass('active');
            $this.attr('data-edit', 'disable');
        } else {
            //on
            $this.addClass('active');
            $this.attr('data-edit', 'enable');
        }

        $editable.editable('toggleDisabled');

    });
});
