
google.load('visualization', '1.0', {'packages':['corechart']});


google.setOnLoadCallback(drawPie);

let _ra, _tp, _sql;

function drawPie(ra, tp, sql) {

    // Create the data table.
    _ra = ra || 0; _tp = tp || 0; _sql = sql || 0;
    let data = google.visualization.arrayToDataTable([
        ['Раздел', '%'],
        ['Алгeбра',     ra],
        ['Кортежи',      tp],
        ['SQL',  sql],
    ]);


    // Set chart options
    var options_bar = {
        title: 'Практика',
        height: 400,
        fontSize: 14,
        colors:['#68b861','#fe915a', '#357eff'],
        chartArea: {
            left: '5%',
            width: '90%',
            //height: 450
        },
    };

    // Instantiate and draw our chart, passing in some options.
    var bar = new google.visualization.PieChart(document.getElementById('pie-chart'));
    bar.draw(data, options_bar);

}


// Resize chart
// ------------------------------

$(function () {

    // Resize chart on menu width change and window resize
    $(window).on('resize', resize);
    $('.menu-toggle').on('click', resize);

    // Resize function
    function resize() {
        drawPie(_ra, _tp, _sql);
    }
});
