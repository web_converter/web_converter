$(document).ready(function () {
    /*$('.grid-div').on('mouseover', function () {
        console.log(this.dataset.me);
    });*/
    let added_elem = [];
    let $info_block = $('#info-block');
    $('.with-info').on('click', function () {
        let $elem = $(this);
        added_elem.map(el => el.remove());

        let info_elem = $info_block
            .clone(true, true)
            .appendTo('body')
            .css('left', $elem.offset().left - $elem.outerWidth() + 'px')
            .css('top', $elem.offset().top + $elem.outerHeight() / 2 + 'px')
            .removeClass('hide')
            .on('click', '#close-info', function () {
                info_elem.addClass('hide');
            });
        added_elem.push(info_elem);
        info_elem.find('#task-info').html(generate_answers_html_grid($elem.data('questions')));
        info_elem.find('#type-info').html($elem.data('isdynamic') ? 'Динамический' : 'Классический');
        info_elem.find('#time-info').html($elem.data('time'));
    });

    function generate_answers_html_grid(answers) {
        if (!answers.length) {
            return '<p class="text-center">Нет ответов</p>';
        }
        let result = '';

        answers.forEach((answer) => {
            result += `<div 
                class="m-0 p-0 float-xs-left text-xs-center" 
                style="width: 1.5rem; height: 1.5rem; 
                    background-color: ${answer.mark > 0 ? 'green' : 'red'}; 
                    border-radius: 3px; border: 1px solid;
                    color: #e0dede;
                    font-weight: bolder;"
                ><span>${answer.mark}</span></div>`;
        });

        return result;
    }
});
